## TEST TERM
1. `TDD` : test driven development (unit test) , thường do developer đảm nhiệm thường là (assert)
2. `BDD`: behavior driven development
   thường là (expect/should)
1. `describe`| `context`: được sử dụng để nhóm test cases và mô tả hành vi của functions/modules/class. Nó nhận hai tham số, tham số đầu tiên mô tả nhóm của bạn và có kiểu dữ liệu là string. Tham số thứ hai là một function callback trong đó bạn có test case hoặc hook functions.
2. `it`| `specify`|`test`: đây là test case, đơn vị test của bạn. Nó phải được mô tả, và các tham số phải chính xác như describe
3. `beforeAll(afterAll)`: hook function chạy trước (sau) tất cả tests. Nó nhận một tham số và function của bạn chạy trước (sau) **tất cả tests**.
4. `beforeEach(afterEach)` :  hook function chạy trước (sau) mỗi tests. Nó nhận một tham số và function của bạn chạy trước (sau) **mỗi tests.**
   > Tham khảo thêm tại : https://docs.cypress.io/guides/core-concepts/writing-and-organizing-tests#Test-Structure


## CYPRESS FRAMEWORK

1. Install
    ```
    npm install --save-dev cypress
    ```
   or use docker image `cypress/base:<nodeVersion>`
2. Run
    ```
        npx cypress open --browser <browserName>
    ```
3. Một số các Matchers trong Cypress tham khảo tại
   >https://docs.cypress.io/guides/references/assertions#Chai
4. Config Cypress tham khảo tại
   >https://docs.cypress.io/guides/references/configuration#cypress-json
5. Trade-off
    1. Automation restrictions : Cypress chỉ là 1 tool kiểm thử ko hỗ trợ
        1. Indexing the web
        2. Spidering links
        3. Performance testing
        4. Scripting 3rd party sites
    2. inside the browser : chỉ hỗ trợ javascript
    3. multiple tabs , browsers open at the same time
    4. Same origin : ko thể vào 2 domain khác nhau trong 1 test case
6. Cypress E2E feature
    1. interacting with element : cypress hỗ trợ các thao tác với các phần tử html như : `click()` , `type()` , `check()`,`trigger`,`selectFile()` . Một số các case test UI được liệt kê practice tham khảo tại
   > https://docs.cypress.io/guides/core-concepts/interacting-with-elements#What-you-ll-learn
   > https://docs.cypress.io/api/commands/and
    2. Khả năng sử dụng Variable & alias  , conditional , đặt debug khi viết test
    3. hỗ trợ nhiều plugin để hỗ trợ kiểm thử các tính năng phức tạp , tương thích với các modern framework Fe
   > https://docs.cypress.io/plugins/directory
7. Cypress DashBoard
    1. CY dashboard cho phép lưu trữ kết quả test bao gồm các thông tin hữu ích như code , record , report flaky test (pricing) ...
    2. mỗi project dashboard có mội `projectId` , và nhiều `recordId`
    3. Dashboard có thể tích hợp với nhiefu nền tảng như Bitbucker , github , gitlab, jira , ...
    4. Smart Orchestation CI (Điều phối)
        1. Paralleztion
        2. Load Balancing
        3. Run fail test firt
        4. Cancel test when a test fali