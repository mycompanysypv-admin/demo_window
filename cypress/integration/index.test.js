describe('test tinh nang login ', function () {
    describe('Test trên desktop', function () {
        beforeEach(()=>{
            cy.visit('http://qlsx.dashboard.vnpt-technology.vn:3000/login')
            cy.viewport(1000,600)
        })
        // Test UI Desktop
        it('Header should match with design ', function () {
            cy.get('.MuiTypography-h1').should('have.css','fontSize','35px')
                .and('have.css','color','rgb(38, 50, 56)')
                .and('have.css','fontWeight','500')
        });
        // Test Logic
        it('UserName field show error msg required ', function () {
            cy.get(':nth-child(2) > .MuiInputBase-root > .MuiInputBase-input').click().blur()
            cy.get('.MuiFormHelperText-root').contains('Bạn cần nhập tên đăng nhập')
                .and('have.css','color','rgb(244, 67, 54)')
            // cy.get('#username').type('admin@gmail.com').and
        });
    });
    describe('Test trên mobile', function () {
        beforeEach(()=>{
            cy.visit('http://qlsx.dashboard.vnpt-technology.vn:3000/login')
            cy.viewport('iphone-x')
        })
        it('Header should match with design ', function () {
            cy.get('.MuiTypography-h1').should('have.css','fontSize','36px')
                .and('have.css','color','rgb(38, 50, 56)')
                .and('have.css','fontWeight','500')
        });

    });


});